export declare class Routers {
    private project;
    private backup;
    route: import("express-serve-static-core").Router;
    constructor(project?: string[], backup?: string[]);
    private paths;
    private Project;
    private Copy;
    private ProjectBack;
    private Backup;
    private Restore;
}
declare const _default: import("express-serve-static-core").Router;
export default _default;
