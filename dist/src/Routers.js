"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routers = void 0;
const express_1 = __importDefault(require("express"));
class Routers {
    constructor(project = ["[ESTIAM] G17 Hackathon"], backup = [""]) {
        this.project = project;
        this.backup = backup;
        this.route = express_1.default.Router({ caseSensitive: true });
        this.Project = () => {
            this.route.get('/projets', (req, res) => {
                //const filenames = fs.readdirSync(`${__dirname}/${req.params.name}`);
                res.status(200).send(this.project);
            });
        };
        this.Copy = () => {
            this.route.get('/copy/:name', (req, res) => {
                this.project.push(`copy-${req.params.name}`);
                res.status(200).send("project has been cloned");
            });
        };
        this.ProjectBack = () => {
            this.route.get('/backups', (req, res) => {
                //const filenames = fs.readdirSync(`${__dirname}/${req.params.name}`);
                res.status(200).send(this.backup);
            });
        };
        this.Backup = () => {
            this.route.get('/backup/:name', (req, res) => {
                this.backup.push(`backup_${req.params.name}_${new Date}`);
                res.status(200).send("the backup was created " + new Date);
            });
        };
        this.Restore = () => {
            this.route.get('/restore/:name', (req, res) => {
                this.project.push(`${req.params.name}`);
                res.status(200).send("the backup has been restored");
            });
        };
        this.paths();
    }
    paths() {
        this.Project();
        this.Copy();
        this.ProjectBack();
        this.Backup();
        this.Restore();
    }
}
exports.Routers = Routers;
exports.default = new Routers().route;
//# sourceMappingURL=Routers.js.map