import express from "express";

export class Routers {

    public route = express.Router({caseSensitive: true})

    constructor(private project = [{name:"[ESTIAM] G17 Hackathon", description: "c'est un projet web"}], private backup: Array<{name:string, description:string}>=[]) {
        this.paths()
    }

    private paths() {
        this.Project()
        this.Copy()
        this.ProjectBack()
        this.Backup()
        this.Restore()
        
        
    }
    
    private Project = (): void => {
        this.route.get('/projets', (req, res) => {
           

            //const filenames = fs.readdirSync(`${__dirname}/${req.params.name}`);
            res.status(200).send(this.project)


        })
    }

    private Copy = (): void => {
        this.route.get('/copy/:name/:description', (req, res) => {
            this.project[this.project.length]={name:`copy-${req.params.name}`,description:`${req.params.description}`}
            res.status(200).send("project has been cloned")


        })
    }
    
    private ProjectBack = (): void => {
        this.route.get('/backups', (req, res) => {
           

            //const filenames = fs.readdirSync(`${__dirname}/${req.params.name}`);
            res.status(200).send(this.backup)


        })
    }

    private Backup = (): void => {
        this.route.get('/backup/:name/:description', (req, res) => {
            this.backup[this.Backup.length]={name:`backup_${req.params.name}_${new Date}`,description:`${req.params.description}`}
            res.status(200).send("the backup was created "+new Date)


        })
    }

    private Restore = (): void => {
        this.route.get('/restore/:name/:description', (req, res) => {
            this.project[this.project.length]={name:`backup_${req.params.name}`,description:`${req.params.description}`}
            res.status(200).send("the backup has been restored")


        })
    }

    

    




}

export default new Routers().route

