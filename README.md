# hackaton fake API

```
npm i
```

```
npm start
```

pour la liste des projets

```
http://localhost:8060/api/v1/projets
```

pour cloner un projet

```
http://localhost:8060/api/v1/copy/[ESTIAM] G17 Hackathon/c'est un projet web
```

pour faire une backup d'un projet

```
http://localhost:8060/api/v1/backup/[ESTIAM]%20G17%20Hackathon/c'est un projet web
```

pour voir les backups

```
http://localhost:8060/api/v1/backups
```

pour restorer une backup

```
http://localhost:8060/api/v1/restore/backup_[ESTIAM]%20G17%20Hackathon_Wed%20Jun%2022%202022%2016:08:51%20GMT+0200%20(heure%20d%E2%80%99%C3%A9t%C3%A9%20d%E2%80%99Europe%20centrale)/c'est un projet web
```
